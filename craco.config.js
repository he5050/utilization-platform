const path = require(`path`);
const CracoEnvPlugin = require('craco-plugin-env')
module.exports = {
  webpack: {
    // 更改build打包文件名称
    configure: (webpackConfig, { env, paths }) => {
      const outFileName = process.env.REACT_APP_FILE_NAME ? process.env.REACT_APP_FILE_NAME : 'euvs'
      webpackConfig.output.path = path.resolve(__dirname, outFileName)
      paths.appBuild = path.resolve(__dirname, outFileName)
      return webpackConfig
    },
    alias: {
      '@': path.resolve(__dirname, 'src/'),
      '@Components': path.resolve(__dirname, 'src/components'),
      '/#/': path.resolve(__dirname, 'types/')
    }
  },
  plugins: [
    {
      plugin: CracoEnvPlugin,
      options: {
        variables: {}
      }
    }
  ]
};