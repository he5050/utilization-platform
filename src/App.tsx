/*
 * @Author: yzr
 * @Date: 2023-09-08 14:25:41
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-11 14:29:04
 * @Description:
 * @FilePath: /utilization/src/App.tsx
 */
import { cloneDeep } from 'lodash-es';
import { RouterProvider } from 'react-router-dom';
import { ConfigProvider, message } from 'antd';
import zhCN from 'antd/locale/zh_CN';
import dayjs from 'dayjs';
import router, { routes } from './router';
import { useMenu } from './store/app';
import theme from '@/style/theme.json';

import 'dayjs/locale/zh-cn';
import './App.css';
import { ConfigProviderProps } from 'antd/es/config-provider';
import { ConfigOptions } from 'antd/es/message/interface';
dayjs.locale('zh-cn');

const messageConfig: ConfigOptions = {
  duration: 5,
  maxCount: 1,
  top: 49,
};

ConfigProvider.config({ theme });
message.config(messageConfig);

const config: ConfigProviderProps = {
  theme,
  locale: zhCN,
  autoInsertSpaceInButton: false,
};

function App() {
  const getMenus = useMenu(state => state.getMenus);
  // 免登录
  const Token = localStorage.getItem('satoken');
  if (Token) {
    window.location.replace('#/');
  }
  getMenus(cloneDeep(routes));
  return (
    <ConfigProvider {...config}>
      <RouterProvider router={router} />
    </ConfigProvider>
  );
}
export default App;
