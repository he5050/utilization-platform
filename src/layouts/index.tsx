/*
 * @Author: yzr
 * @Date: 2023-09-08 14:25:41
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-19 11:03:03
 * @Description:
 * @FilePath: /utilization/src/layouts/index.tsx
 */
import { Layout } from 'antd';
import LayoutHeader from './header';
import LayoutSider from './sider';
import BreadcrumbSelf from './breadcrumb';
import { Outlet, Navigate, useLocation } from 'react-router-dom';

const { Content } = Layout;

const Layouts: React.FC = () => {
  let location = useLocation();
  let shouldRedirect = location.pathname === '/';

  return (
    <>
      {shouldRedirect && <Navigate to="/deviceStatus" />}
      <Layout style={{ height: '100vh' }}>
        <LayoutHeader />
        <Layout>
          <LayoutSider />
          <Layout style={{ marginLeft: '200px' }}>
            <Content className="flex-column">
              <BreadcrumbSelf />
              <Outlet />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </>
  );
};

export default Layouts;
