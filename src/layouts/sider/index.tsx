import React from 'react';
import { Layout } from 'antd';
import LayoutMenu from '../menu';

const { Sider } = Layout;

const LayoutSider: React.FC = () => {
  return (
    <Sider
      theme="light"
      width={200}
      style={{
        overflowY: 'auto',
        overflowX: 'hidden',
        height: 'calc(100vh - 48px)',
        position: 'fixed',
        left: 0,
        top: '48px',
        bottom: 0,
      }}
    >
      <LayoutMenu></LayoutMenu>
    </Sider>
  );
};

export default LayoutSider;
