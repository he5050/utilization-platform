/*
 * @Author: yzr
 * @Date: 2023-09-20 09:57:43
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-20 10:26:06
 * @Description:
 * @FilePath: /utilization/src/http/error.ts
 */
// http错误拦截器
import { message } from 'antd';
import { HttpStatusCode } from 'axios';
import type { AxiosError } from 'axios';
import { ResponseData } from '../types/axios.d';

const errorInterceptor = (error: AxiosError<ResponseData>) => {
  const { response } = error;
  let msg = '';

  switch (response?.status) {
    case HttpStatusCode.BadGateway:
      msg = '错误的请求方法';
      break;
    case HttpStatusCode.ServiceUnavailable:
      msg = '请稍后请求';
      break;
    case HttpStatusCode.RequestTimeout:
      msg = '请求超时';
      break;
    case HttpStatusCode.GatewayTimeout:
      msg = '服务器超时';
      break;
    case HttpStatusCode.NotFound:
      msg = '资源不存在';
      break;
    case HttpStatusCode.Unauthorized:
      msg = '未授权';
      return;
    case HttpStatusCode.Forbidden:
      msg = '请求被拒绝';
      break;
    case HttpStatusCode.BadRequest:
      msg = '请求参数错误';
      break;
  }

  response && message.error(msg);
  return Promise.resolve(response?.data);
};

export default errorInterceptor;
