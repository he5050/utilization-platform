import request from '@/http';
const api = {
  deviceStatusStatistics: '/monitorView/deviceStatusStatistics', //设备状态统计
  devExceptionStatusTrend: '/monitorView/devExceptionStatusTrend', //设备异常变化趋势统计
  deviceDeptCount: '/device/deviceDeptCount', //部门设备统计数
  deviceMonthlyIncrCount: '/device/deviceMonthlyIncrCount', //设备月增量
  deviceTypeStatistics: '/deviceType/deviceTypeStatistics', //设备类型统计
};

/**
 * 设备状态统计
 */
export interface DeviceStatusStatisticInterface {
  /**
   * 部门数量
   */
  deptCount?: number;
  /**
   * 设备数量
   */
  deviceCount?: number;
  /**
   * 设备月增量
   */
  deviceMonthlyIncr?: number;
  /**
   * 设备月增量比例
   */
  deviceMonthlyIncrScale?: string;
  /**
   * 设备当月离线Top5
   */
  deviceOfflineStatistics?: DeviceTimeAbnormal[];
  /**
   * 设备当月关机Top5
   */
  deviceShutDownStatistics?: DeviceTimeAbnormal[];
  /**
   * 设备类型数量
   */
  deviceTypeCount?: number;
  [property: string]: any;
}

/**
 * 设备异常时长统计
 */
export interface DeviceTimeAbnormal {
  /**
   * 部门
   */
  department?: string;
  /**
   * 设备编码
   */
  deviceCode?: string;
  /**
   * 时长
   */
  duration?: number;
  /**
   * 型号
   */
  model?: string;
  /**
   * 设备类型
   */
  type?: string;
}

/**
 * 设备异常状态趋势
 */
export interface ExceptionStatus {
  /**
   * 设备编码
   */
  deviceCode?: string;
  /**
   * 结束时间 格式:yyyy-MM-dd
   */
  endTime?: Date | string;
  /**
   * 开始时间 格式:yyyy-MM-dd
   */
  startTime?: Date | string;
  /**
   * 时间维度类型 day:按日 mouth:按月 quarter:按季度 year:按年
   */
  timeDimensionType?: string;
  /**
   * 工作状态 -10:累计离线时长 0:累计关机时长 10:累计待机时长
   */
  workStatus?: number;
}

/* 获取设备状态统计 */
export const getDeviceStatusStatistics = () => {
  return request.get<{ data: DeviceStatusStatisticInterface }>({
    url: api.deviceStatusStatistics,
  });
};
/* 获取设备异常变化趋势统计 */
export const getDevExceptionStatusTrend = (
  data: ExceptionStatus,
  options?: { [key: string]: any }
) => {
  return request.post<{ data: any }>(
    {
      url: api.devExceptionStatusTrend,
      ...(options || {}),
    },
    data
  );
};

/* 获取设备类型统计 */
export const getDeviceTypeStatistics = (params: { isDeviceExist?: number }) => {
  return request.get(
    {
      url: api.deviceTypeStatistics,
    },
    {
      params,
    }
  );
};
/* 获取设备月增量统计 */
export const getDeviceMonthlyIncrCount = () => {
  return request.get({
    url: api.deviceMonthlyIncrCount,
  });
};
/* 按照部门统计设备 */
export const getDeviceDeptCount = () => {
  return request.get({
    url: api.deviceDeptCount,
  });
};
