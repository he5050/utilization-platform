import request from '@/http';

// 新接口
export function getUtilizationRate(options?: { [key: string]: any }) {
  return request.get<{ data: API.monthlyRate }>({
    url: '/analysisStatistics/queryMonthUtilizationRate',
    ...(options || {}),
  });
}

export function getLowRate(
  data?: { department?: string[]; deviceType?: string[] },
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.deviceRate[] }>(
    {
      url: '/analysisStatistics/queryLowDeviceUtilizationRates',
      ...(options || {}),
    },
    data
  );
}

export function getHighRate(
  data?: { department?: string[]; deviceType?: string[] },
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.deviceRate[] }>(
    {
      url: '/analysisStatistics/queryHighDeviceUtilizationRates',
      ...(options || {}),
    },
    data
  );
}
// 利用率趋势
export function getUtilRateTrend(
  data: API.TrendReq,
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.TrendData }>(
    {
      url: '/analysisStatistics/queryUtilizationRateTrend',
      ...(options || {}),
    },
    data
  );
}
export function exportUtilRateTrebd(
  data: API.TrendReq,
  options?: { [key: string]: any }
) {
  return request.post<{ data: any }>(
    {
      url: '/analysisStatistics/exportUtilizationRateTrend',
      ...(options || {}),
      responseType: 'arraybuffer',
    },
    data
  );
}

// 利用率详情列表
export function getWorkRecordsAPI(
  data: API.WorkRecord,
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.WorkRecordResult }>(
    {
      url: '/monitorView/deviceTimeDayPage',
      ...(options || {}),
    },
    data
  );
}
// 利用率工作详情
export function getWorkDetailAPI(
  data: API.WorkRecord,
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.WorkDetailResult[] }>(
    {
      url: '/monitorView/getWorkList',
      ...(options || {}),
    },
    data
  );
}
// 导出利用率详情
export function exportWorkRecordAPI(
  data: API.WorkParams,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: '/monitorView/exportWorkByDay',
      ...(options || {}),
      headers: { passInterceptors: true, intact: true },
      responseType: 'arraybuffer',
    },
    data
  );
}
