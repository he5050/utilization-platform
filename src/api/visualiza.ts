import request from '@/http';

export function analysisConfigListAPI(
  data: API.AnalysisConfigList,
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.AnalysisConfigResult[] }>(
    {
      url: '/configInfo/pageList',
      ...(options || {}),
    },
    data
  );
}

export function updateRuleConfigAPI(
  data: { collectionDeviceCode?: string; configValue?: string },
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: '/configInfo/updateJsonValue',
      ...(options || {}),
    },
    data
  );
}

export function getDayPowerAPI(
  data: API.DevicePowerParams,
  options?: { [key: string]: any }
) {
  return request.post<{ data: { records: API.DevicePowerResult[] } }>(
    {
      url: `/configInfo/iotHistoryDataQuery`,
      ...(options || {}),
    },
    data
  );
}

export function getUnbindDeviceAPI(
  params: { collectionDeviceCode: string },
  options?: { [key: string]: any }
) {
  return request.get<{ data: string[] }>(
    {
      url: `/configInfo/unbindConfigDeviceList`,
      ...(options || {}),
    },
    { params }
  );
}

export function addRuleConfigAPI(
  data: API.AddConfigParams,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/configInfo/saveConfig`,
      ...(options || {}),
    },
    data
  );
}

// 根据设备档案编码获取配置
export function getConfigByDeviceCode(
  params: { deviceCode: string },
  options?: { [key: string]: any }
) {
  return request.get<{ data: API.RuleConfigResult }>(
    {
      url: `/configInfo/configByDeviceCode`,
      ...(options || {}),
    },
    { params }
  );
}

// 重算结果
export function recalculateResultAPI(
  data: { deviceCode: string; recalculationDate: string },
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.RecalculateResult }>(
    {
      url: `/recalculate/getRecalculateWorkRecordDetail`,
      ...(options || {}),
    },
    data
  );
}

// 重算
export function recalculateAPI(
  data: API.RecalculateParams,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/recalculate/restart`,
      ...(options || {}),
    },
    data
  );
}
type ID = {
  id: string;
};
export function getLedgerTableAPI(
  data: API.LedgerTableParams,
  options?: { [key: string]: any }
) {
  return request.post<{ data: (API.LedgerTable & ID)[] }>(
    {
      url: `/device/page`,
      ...(options || {}),
    },
    data
  );
}

export function addDeviceLedgerAPI(
  data: API.LedgerTable,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/device/save`,
      ...(options || {}),
    },
    data
  );
}
// 批量新增
export function addMultiDeviceLedgerAPI(
  fileUpName?: Blob,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/device/upload`,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      ...(options || {}),
    },
    { fileUpName }
  );
}

export function updateDeviceLedgerAPI(
  data: API.LedgerTable & { id: string },
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/device/update`,
      ...(options || {}),
    },
    data
  );
}
export function delDeviceLedgerAPI(
  params: { id: string },
  options?: { [key: string]: any }
) {
  return request.get(
    {
      url: `/device/del`,
      ...(options || {}),
    },
    { params }
  );
}

export function exportDeviceLedgerAPI(
  data: API.LedgerTableParams,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/device/export`,
      ...(options || {}),
      headers: { passInterceptors: true, intact: true },
      responseType: 'arraybuffer',
    },
    data
  );
}

export function bindSensorAPI(
  data: API.BindSensorParams,
  options?: { [key: string]: any }
) {
  return request.post(
    {
      url: `/configInfo/bindCollectionDevice`,
      ...(options || {}),
    },
    data
  );
}

export function unBindSensorAPI(
  params: API.UnBindSensorParams,
  options?: { [key: string]: any }
) {
  return request.get(
    {
      url: `/configInfo/unbindCollectionDevice`,
      ...(options || {}),
    },
    { params }
  );
}

//重新计算保存数据
export function saveRecalculateData(
  data: API.RuleConfig,
  options?: { [key: string]: any }
) {
  return request.post<{ data: string[] }>(
    {
      url: `/configInfo/saveRecalculateData`,
      ...(options || {}),
    },
    data
  );
}
