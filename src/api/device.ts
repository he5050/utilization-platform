/*
 * @Author: yzr
 * @Date: 2023-09-20 13:54:11
 * @LastEditors: yzr
 * @LastEditTime: 2023-10-08 11:06:53
 * @Description: 设备相关接口
 * @FilePath: /utilization/src/api/device.ts
 */
import request from '@/http';

// 设备类型列表
export function getDeviceTypeList(
  data: {
    page?: number;
    pageSize?: number;
    searchKey?: string;
    sortItems?: { asc: boolean; column: string }[];
  },
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.deviceDetails }>(
    {
      url: '/deviceType/pageList',
      ...(options || {}),
    },
    data
  );
}

// 设备列表
export function getDeviceList(
  data: {
    page?: number;
    pageSize?: number;
    departments?: string[]; // 部门
    deviceTypes?: string[]; // 设备类型
    searchKey?: string; // 关键词
    sortItems?: { asc: boolean; column: string }[];
    workStatus?: number[]; // 工作状态   -10:离线 0：关机 10：待机 20：工作
  },
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.deviceList }>(
    {
      url: '/device/page',
      ...(options || {}),
    },
    data
  );
}

// 设备型号列表
export function getDeviceModels(
  params?: { deviceType?: string },
  options?: { [key: string]: any }
) {
  return request.get<{ data: { model: string; [property: string]: any } }>(
    {
      url: '/device/queryDeviceModels',
      ...(options || {}),
    },
    { params }
  );
}

//部门列表
export function getDepList(
  params?: { deviceType?: string },
  options?: { [key: string]: any }
) {
  return request.get<{ data: string[] }>(
    {
      url: '/device/departmentList',
      ...(options || {}),
    },
    { params }
  );
}

// 设备编码模糊查询
export function queryDeviceByCode(
  data: { department?: string; deviceType?: string; searchKey?: string },
  options?: { [key: string]: any }
) {
  return request.post<{ data: string[] }>(
    {
      url: '/device/deviceCodeQuery',
      ...(options || {}),
    },
    data
  );
}
