/*
 * @Author: qingyuanyang
 * @Date: 2023-09-21 19:37:35
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-09-22 16:31:21
 * @FilePath: /utilization-platform/src/api/deviceInstrument.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/http';
const api = {
  instrumentList: '/deviceType/pageList', //设备仪器列表
  instrumentDelete: '/deviceType/del', //删除
  instrumentEdit: '/deviceType/saveOrUpdate', //新增/编辑
};
export interface InstrumentResponse {
  /**
   * 当前页码
   */
  page?: number;
  /**
   * 每页显示条数，默认 10
   */
  pageSize?: number;
  /**
   * 当前页记录
   */
  records?: InstrumentDetails[];
  /**
   * 总记录数
   */
  total?: number;
  /**
   * 总页数
   */
  totalPages?: number;
  [property: string]: any;
}

export interface InstrumentDetails {
  /**
   * 创建人
   */
  createBy?: string;
  /**
   * 创建人id
   */
  createId?: number;
  /**
   * 创建时间
   */
  createTime?: Date;
  /**
   * 设备类型 租户内唯一
   */
  deviceType?: string;
  /**
   * 主键
   */
  id?: number;
  /**
   * 是否逻辑删除，0否,其他是
   */
  isDelete?: number;
  /**
   * 别名
   */
  name?: string;
  /**
   * 计划利用率
   */
  plannedUtilization?: number;
  /**
   * 状态细分是否启用0:不启用1:启用
   */
  statusSegmentationEnable?: number;
  /**
   * 租户编号
   */
  tenantId?: number;
  /**
   * 修改人
   */
  updateBy?: string;
  /**
   * 修改人id
   */
  updateId?: number;
  /**
   * 更新时间
   */
  updateTime?: Date;
  /**
   * 利用率分母 默认24
   */
  utilizationDenominator?: number;
  /**
   * 利用率高阈值
   */
  utilizationHighThreshold?: number;
  /**
   * 利用率低阈值
   */
  utilizationLowThreshold?: number;
  /**
   * 乐观锁版本号
   */
  version?: number;
  /**
   * 当前页码，分页查询时传参数，列表查询不需要传
   */
  page?: number;
  /**
   * 每页记录数，分页查询时传参数，列表查询不需要传
   */
  pageSize?: number;
  /**
   * 关键词查询
   */
  searchKey?: string;
  /**
   * 排序字段列表
   */
  sortItems?: [
    {
      /**
       * 排序方向：true升序，fase降序
       */
      asc?: boolean;
      /**
       * 需要进行排序的字段
       */
      column?: string;
    }
  ];
}
/**
 * 设备类型分页查询
 */
export interface Params {
  /**
   * 当前页码，分页查询时传参数，列表查询不需要传
   */
  page?: number;
  /**
   * 每页记录数，分页查询时传参数，列表查询不需要传
   */
  pageSize?: number;
  /**
   * 关键词查询
   */
  searchKey?: string;
  /**
   * 排序字段列表
   */
  sortItems?: [
    {
      /**
       * 排序方向：true升序，fase降序
       */
      asc?: boolean;
      /**
       * 需要进行排序的字段
       */
      column?: string;
    }
  ];
}

export const getInstrumentList = (data: Params) => {
  return request.post(
    {
      url: api.instrumentList,
    },
    data
  );
};
export const EditInstrument = (data: InstrumentDetails) => {
  return request.post(
    {
      url: api.instrumentEdit,
    },
    data
  );
};
export const deleteInstrument = (params: InstrumentDetails) => {
  return request.get(
    {
      url: api.instrumentDelete,
    },
    { params }
  );
};
