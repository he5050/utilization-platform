/*
 * @Author: yzr
 * @Date: 2023-09-13 14:24:10
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-20 13:27:57
 * @Description:
 * @FilePath: /utilization/src/api/login.ts
 */
import request from '@/http';

export function loginAPI(data: API.LoginReq, options?: { [key: string]: any }) {
  return request.post<{ data: API.LoginRes }>(
    {
      url: '/login/doLogin',
      ...(options || {}),
    },
    data
  );
}

export function loginOutAPI(data?: any, options?: { [key: string]: any }) {
  return request.get<{ data: API.LoginRes }>(
    {
      url: '/login/logout',
      ...(options || {}),
    },
    data
  );
}

export function updatePasswordAPI(
  data: API.updatePasswordReq,
  options?: { [key: string]: any }
) {
  return request.post<{ data: API.LoginRes }>(
    {
      url: '/login/updatePassWord',
      ...(options || {}),
    },
    data
  );
}
