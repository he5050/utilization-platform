declare namespace API {
  type LocationTree = {
    id?: number;
    name?: string;
    type?: number;
    disabled?: boolean;
    children?: LocationTree[];
  };
  type DeviceParams = {
    locationId: number;
    recursive?: number;
  };
  type DeviceCode = {
    code: string;
  };
  type DeviceType = {
    deviceType: string;
    devCount: number;
  };
  type PageParams = {
    page?: number;
    pageSize?: number;
  };
  type MonitorList = PageParams & {
    collectionDeviceCodes?: string[];
    deviceTypes?: string[];
    workStatus?: number;
  };
  type MonitorListResult = {
    collectionProductCode?: string;
    collectionDeviceCode?: string;
    collectionDeviceName?: string;
    deviceCode?: string;
    electricCurrent?: string;
    workStatus?: number;
    onlineStatusString?: string;
    type?: string;
    power?: string;
    voltage?: string;
    picInfo?: string;
  };
  type WorkRecord = PageParams & {
    deviceCode?: string;
    startTime?: string;
    endTime?: string;
  };
  interface WorkRecordResult {
    /**
     * 当前页码
     */
    page?: number;
    /**
     * 每页显示条数，默认 10
     */
    pageSize?: number;
    /**
     * 当前页记录
     */
    records?: WorkResultRecords[];
    /**
     * 总记录数
     */
    total?: number;
    /**
     * 总页数
     */
    totalPages?: number;
    [property: string]: any;
  }

  /**
   * DeviceTimeDay对象，设备利用率天统计
   */
  interface WorkResultRecords {
    /**
     * 创建人
     */
    createBy?: string;
    /**
     * 创建人id
     */
    createId?: number;
    /**
     * 创建时间
     */
    createTime?: Date;
    /**
     * 累计工作时长 单位秒
     */
    cumulativeWorkingHours?: number;
    /**
     * 累计工作次数
     */
    cumulativeWorkingNum?: number;
    /**
     * 设备编号 租户内唯一
     */
    deviceCode?: string;
    /**
     * 主键
     */
    id?: number;
    /**
     * 是否逻辑删除，0否,其他是
     */
    isDelete?: number;
    /**
     * 统计时间
     */
    statisticsTime?: Date;
    /**
     * 租户编号
     */
    tenantId?: number;
    /**
     * 修改人
     */
    updateBy?: string;
    /**
     * 修改人id
     */
    updateId?: number;
    /**
     * 更新时间
     */
    updateTime?: Date;
    /**
     * 利用率 保留三位小数
     */
    utilization?: number;
    /**
     * 利用率分母
     */
    utilizationDenominator?: number;
    /**
     * 乐观锁版本号
     */
    version?: number;
  }
  type DeviceDetailResult = {
    picInfo: string;
    collectionDeviceCode: string;
    collectionProductCode: string;
    deviceCode: string;
    voltage: string;
    electricCurrent: string;
    power: string;
    children: DeviceDetailResult[] | null;
  };
  type WorkDetailResult = {
    startTime?: string;
    endTime?: string;
    duration: number;
    validDuration: number;
  };
  type WorkParams = {
    startTime?: string;
    endTime?: string;
    deviceCode?: string;
  };
  type WorkStatistics = {
    dayInfo?: string | number;
    result?: string | number;
  };
  type WorkTimeStatistics = {
    standbyStatus?: WorkStatistics[];
    workStatus?: WorkStatistics[];
  };
  type WorkAnalysis = {
    type?: string | number;
    result?: string | number;
  };
  type DeviceOverview = {
    analysisType: number;
    startTime?: string;
    endTime?: string;
    type?: string[];
  };
  type SingleDeviceOverview = Omit<DeviceOverview, 'type'> & {
    type?: string;
  };

  // analysis config
  type AnalysisConfigList = PageParams & {
    deviceType?: string;
    searchKey?: string;
  };

  type AnalysisConfigResult = {
    id?: string;
    type?: string;
    deviceCode?: string;
    collectionDeviceCode?: string;
    productCode?: string;
    monitorMethod?: string;
    configRemark?: string;
    children?: AnalysisConfigResult[] | null;
  };

  type RuleConfig = {
    deviceCodes?: string[];
    recalculationDate?: string;
  };

  type RuleConfigResult = {
    configInfoList?: {
      // 档案编码
      archivesCode: string;
      // 传感器编码
      deviceCode: string;
      configValue: string;
      productCode: string;
    }[];
    // 传感器编码
    collectionDeviceCode?: string;
    // 档案编码
    deviceCode?: string;
    collectionProductCode?: string;
    deviceTypeDetails?: DeviceTypeDetails;
  };
  /**
   * DeviceTypeDetails对象
   */
  type DeviceTypeDetails = {
    /**
     * 创建人
     */
    createBy?: string;
    /**
     * 创建人id
     */
    createId?: number;
    /**
     * 创建时间
     */
    createTime?: Date;
    /**
     * 设备类型 租户内唯一
     */
    deviceType?: string;
    /**
     * 主键
     */
    id?: number;
    /**
     * 是否逻辑删除，0否,其他是
     */
    isDelete?: number;
    /**
     * 别名
     */
    name?: string;
    /**
     * 计划利用率
     */
    plannedUtilization?: number;
    /**
     * 状态细分是否启用0:不启用1:启用
     */
    statusSegmentationEnable?: number;
    /**
     * 租户编号
     */
    tenantId?: number;
    /**
     * 修改人
     */
    updateBy?: string;
    /**
     * 修改人id
     */
    updateId?: number;
    /**
     * 更新时间
     */
    updateTime?: Date;
    /**
     * 利用率分母 默认24
     */
    utilizationDenominator?: number;
    /**
     * 利用率高阈值
     */
    utilizationHighThreshold?: number;
    /**
     * 利用率低阈值
     */
    utilizationLowThreshold?: number;
    /**
     * 乐观锁版本号
     */
    version?: number;
    [property: string]: any;
  };

  type DevicePowerParams = {
    attributeName: string;
    type: number;
    deviceCode?: string;
    endTime?: string;
    productCode?: string;
    startTime?: string;
    page?: number;
    pageSize?: number;
  };

  type DevicePowerResult = {
    time?: string;
    value?: number;
  };

  type AddConfigParams = {
    configName?: string;
    configRemark?: string;
    deviceCode?: string;
    monitorMethod?: string;
  };

  type StatisticsParams = {
    analysisType?: number;
    startTime?: string;
    endTime?: string;
    type?: string[];
  };

  type UseTopParams = {
    analysisType?: number;
    startTime?: string;
    endTime?: string;
    topNum?: string;
    timeDimensionType?: 'day' | 'week' | 'month' | 'quarter';
  };

  type deviceTypeResult = {
    devCount?: number;
    deviceType?: string;
  };

  type DeviceTopResult = {
    dayInfo: string;
    result: string;
    type: string;
  };
  type SingleDeviceTypeAnalysisResult = {
    recordsMap: {
      [key in string]: API.DeviceTopResult[];
    };
    typeCounts: {
      [key in string]: number;
    };
  };

  type RecalculateParams = {
    params: {
      collectionDeviceCode?: string;
      configValue?: string;
      deviceCode: string;
    }[];
    recalculationDate: string;
  };

  type RecalculateResult = {
    deviceTimeBuckerRecalculateMap?: Record<string, WorkDetailResult[]>;
    recalculateWorkRecordMap?: Record<
      string,
      null | {
        configValue?: string;
        status?: number;
        recalculationDate?: string;
      }
    >;
  };

  type LedgerTableParams = {
    deviceTypes?: string[];
    workStatus?: number[];
    searchKey?: string;
    page?: number;
    pageSize?: number;
  };

  type LedgerTable = {
    bar?: string;
    columnOven?: string;
    department?: string;
    detector?: string;
    deviceCode?: string;
    injector?: string;
    isImportant?: number;
    manufacturer?: string;
    model?: string;
    onlineStatusString?: string;
    principal?: string;
    pump?: string;
    remark?: string;
    software?: string;
    type?: string;
    workStatus?: number;
    workStatusString?: string;
  };

  type BindSensorParams = {
    collectionDeviceCodes: string[];
    deviceCode?: string;
  };
  type UnBindSensorParams = {
    collectionDeviceCode: string;
    deviceCode?: string;
  };
  type UtilizationStatisticParams = {
    departments: string[];
    types: string[];
    startTime: string;
    endTime: string;
  };
  type DepartmentUtilizationResult = {
    deptCount: number;
    devCount: number;
    utilizationRateStatisticsList: {
      deptName: string;
      utilizationRate: string;
    }[];
  };

  /**
   * 用户登录参数
   */
  type LoginReq = {
    /**
     * 密码
     */
    password?: string;
    /**
     * 用户名
     */
    userName?: string;
  };
  interface TokenInfo {
    isLogin: boolean;
    loginDevice: string;
    loginId: string;
    loginType: string;
    sessionTimeout: number;
    tag: string;
    tokenActiveTimeout: number;
    tokenName: string;
    tokenSessionTimeout: number;
    tokenTimeout: number;
    tokenValue: string;
  }
  /**
   * api接口返回对象，api接口返回对象
   */
  interface LoginRes {
    /**
     * 响应结果
     */
    data: {
      tokenInfo: TokenInfo;
      userId: string;
      userName: string;
    };
    /**
     * 响应代码
     */
    errorCode?: number;
    /**
     * 响应消息
     */
    errorMsg?: string;
    /**
     * 成功标志
     */
    success?: boolean;
    /**
     * 时间戳
     */
    timestamp?: number;
  }
  /**
   * 修改密码请求体
   */
  type updatePasswordReq = {
    /**
     * 新密码
     */
    newPassword?: string;
    /**
     * 原密码
     */
    oldPassword?: string;
  };

  /**
   * 月利用率
   */
  interface monthlyRate {
    /**
     * 平均利用率
     */
    averageUtilizationRate?: number;
    maximumDepartmentUtilizationRate?: depRate;
    minimumDepartmentUtilizationRate?: depRate;
  }

  /**
   * 部门利用率
   */
  interface depRate {
    /**
     * 部门
     */
    department?: string;
    /**
     * 利用率
     */
    utilizationRate?: number;
  }

  interface deviceRate {
    /**
     * 部门
     */
    department?: string;
    /**
     * 设备编号
     */
    deviceCode?: string;
    /**
     * 型号
     */
    deviceModel?: string;
    /**
     * 设备类型
     */
    deviceType?: string;
    /**
     * 利用率高阈值
     */
    highUtilizationThreshold?: number;
    /**
     * 利用率低阈值
     */
    lowUtilizationThreshold?: number;
    /**
     * 利用率
     */
    utilizationRate?: number;
  }

  /**
   * 分页对象«DeviceTypeDetails对象»，分页对象
   */
  interface deviceDetails {
    /**
     * 当前页码
     */
    page?: number;
    /**
     * 每页显示条数，默认 10
     */
    pageSize?: number;
    /**
     * 当前页记录
     */
    records?: deviceRecord[];
    /**
     * 总记录数
     */
    total?: number;
    /**
     * 总页数
     */
    totalPages?: number;
    [property: string]: any;
  }

  /**
   * DeviceTypeDetails对象
   */
  interface deviceRecord {
    /**
     * 创建人
     */
    createBy?: string;
    /**
     * 创建人id
     */
    createId?: number;
    /**
     * 创建时间
     */
    createTime?: Date;
    /**
     * 设备类型 租户内唯一
     */
    deviceType?: string;
    /**
     * 主键
     */
    id?: number;
    /**
     * 是否逻辑删除，0否,其他是
     */
    isDelete?: number;
    /**
     * 别名
     */
    name?: string;
    /**
     * 计划利用率
     */
    plannedUtilization?: number;
    /**
     * 状态细分是否启用0:不启用1:启用
     */
    statusSegmentationEnable?: number;
    /**
     * 租户编号
     */
    tenantId?: number;
    /**
     * 修改人
     */
    updateBy?: string;
    /**
     * 修改人id
     */
    updateId?: number;
    /**
     * 更新时间
     */
    updateTime?: Date;
    /**
     * 利用率分母 默认24
     */
    utilizationDenominator?: number;
    /**
     * 利用率高阈值
     */
    utilizationHighThreshold?: number;
    /**
     * 利用率低阈值
     */
    utilizationLowThreshold?: number;
    /**
     * 乐观锁版本号
     */
    version?: number;
    [property: string]: any;
  }

  /**
   * 分页对象«设备属性列表»，分页对象
   */
  interface deviceList {
    /**
     * 当前页码
     */
    page?: number;
    /**
     * 每页显示条数，默认 10
     */
    pageSize?: number;
    /**
     * 当前页记录
     */
    records?: deviceRecords[];
    /**
     * 总记录数
     */
    total?: number;
    /**
     * 总页数
     */
    totalPages?: number;
    [property: string]: any;
  }

  /**
   * 设备属性列表
   */
  interface deviceRecords {
    /**
     * 采集时间
     */
    collectionDateTime?: string;
    /**
     * 采集设备编码
     */
    collectionDeviceCode?: string;
    /**
     * 采集设备名称 用于保存注册编码
     */
    collectionDeviceName?: string;
    /**
     * 采集设备类编码
     */
    collectionProductCode?: string;
    /**
     * 部门
     */
    department?: string;
    /**
     * 设备编号 租户内唯一
     */
    deviceCode?: string;
    /**
     * 录入时间
     */
    entryTime?: Date;
    /**
     * 主键ID
     */
    id?: number;
    /**
     * 数据最后上报时间 用于和当前时间比较判断在线状态
     */
    lastDataTime?: number;
    /**
     * 厂家
     */
    manufacturer?: string;
    /**
     * 型号
     */
    model?: string;
    /**
     * 父ID
     */
    parentId?: number;
    /**
     * 设备图片
     */
    picInfo?: string;
    /**
     * 备注
     */
    remark?: string;
    /**
     * 设备类型
     */
    type?: string;
    /**
     * 工作状态 0：关机  10：待机 20：工作
     */
    workStatus?: number;
    /**
     * 工作状态
     */
    workStatusString?: string;
    [property: string]: any;
  }

  interface TrendReq {
    /**
     * 对比维度，枚举（dept：部门、device：设备、model：型号）
     */
    comparisonDimension?: string;
    /**
     * 对比项目
     */
    comparisonItems?: string[];
    /**
     * 设备类型
     */
    deviceType: string;
    /**
     * 结束时间，格式（按天：yyyy-MM-dd、按月：yyyy-MM、按季度：yyyy-Qn、按年：yyyy）
     */
    endTime: string;
    /**
     * 开始时间，格式（按天：yyyy-MM-dd、按月：yyyy-MM、按季度：yyyy-Qn、按年：yyyy）
     */
    startTime: string;
    /**
     * 时间维度，枚举（day：按天、month：按月、quarter：按季度、year：按年）
     */
    timeDimensionType: string;
    [property: string]: any;
  }
  /**
   * 利用率趋势
   */
  interface TrendData {
    /**
     * 对比项目利用率
     */
    comparisonItemRates: ComparisonItemRate[];
    /**
     * 计划利用率，百分比显示时需要乘以100%
     */
    plannedUtilization: number;
    [property: string]: any;
  }

  /**
   * ComparisonItemRate
   */
  interface ComparisonItemRate {
    /**
     * 对比项目
     */
    comparisonItem: string;
    /**
     * 利用率
     */
    utilizationRates: TimeUtilizationRate[];
    [property: string]: any;
  }

  /**
   * TimeUtilizationRate
   */
  interface TimeUtilizationRate {
    /**
     * 对比项目
     */
    comparisonItem: string;
    /**
     * 统计时间
     */
    statisticsTime: string;
    /**
     * 利用率，百分比显示时需要乘以100%
     */
    utilizationRate: number;
    [property: string]: any;
  }
}
