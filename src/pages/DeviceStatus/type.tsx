import { DeviceTimeAbnormal } from '@/api/deviceStatus';
import StatusIconOne from '@/assets/image/device-status/status-icon1.png';
import StatusIconTwo from '@/assets/image/device-status/status-icon2.png';
import StatusIconThree from '@/assets/image/device-status/status-icon3.png';
import StatusIconFour from '@/assets/image/device-status/status-icon4.png';
import Ellipsis from '@/components/ellipsis-text';
import { SelectProps } from 'antd';
import { ColumnsType } from 'antd/es/table';
export interface TabType {
  id: number;
  num?: number;
  text: string;
  img: any;
  textColor: string;
  bgColor: string;
  tip?: string;
  percent?: string;
}
export const tabItem: TabType[] = [
  {
    id: 1,
    num: 100,
    text: '设备数量',
    img: StatusIconOne,
    textColor: '#3471FF',
    bgColor: 'rgba(52, 134, 255, 0.12)',
  },
  {
    id: 2,
    num: 100,
    text: '设备类型',
    img: StatusIconTwo,
    textColor: '#0DC191',
    bgColor: 'rgba(13, 193, 145, 0.12)',
  },
  {
    id: 3,
    num: 100,
    text: '使用部门数量',
    img: StatusIconThree,
    textColor: '#3471FF',
    bgColor: 'rgba(50, 35, 246, 0.12)',
  },
  {
    id: 4,
    num: 100,
    text: '设备月增量',
    img: StatusIconFour,
    textColor: '#FA8C16',
    bgColor: 'rgba(250, 140, 22, 0.12)',
    tip: '本月和上月设备数量对比',
  },
];
export const deviceColumns: ColumnsType<DeviceTimeAbnormal> = [
  {
    title: '设备编号',
    dataIndex: 'deviceCode',
    key: 'deviceCode',
    ellipsis: true,
    render: value => <Ellipsis content={value} />,
  },
  {
    title: '设备类型',
    dataIndex: 'type',
    key: 'type',
    ellipsis: true,
    render: value => <Ellipsis content={value} />,
  },
  {
    title: '部门',
    dataIndex: 'department',
    key: 'department',
    ellipsis: true,
    render: value => <Ellipsis content={value} />,
  },
  {
    title: '型号',
    dataIndex: 'model',
    key: 'model',
    ellipsis: true,
    render: value => <Ellipsis content={value} />,
  },
];

export interface Label {
  value?: number;
  label?: string;
}

/*状态 -10:累计离线时长 0:累计关机时长 10:累计待机时长 */

export const dimensionTypeOptions = [
  {
    value: 0,
    label: '累计关机时长',
  },
  {
    value: -10,
    label: '累计离线时长',
  },
  {
    value: 10,
    label: '累计待机时长',
  },
];
export const dateOptions: SelectProps['options'] = [
  {
    label: '按日',
    value: 'day',
  },
  {
    label: '按月',
    value: 'month',
  },
  {
    label: '按季度',
    value: 'quarter',
  },
  {
    label: '按年',
    value: 'year',
  },
];

/**
 * 设备tab数量统计
 */
export interface DeviceTabType {
  /**
   * 部门
   */
  department?: string;
  /**
   * 设备数量
   */
  devCount?: number;
  /**
   * 设备类型
   */
  deviceType?: string;
}
