/*
 * @Author: qingyuanyang
 * @Date: 2023-09-20 15:53:26
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-09-26 10:29:14
 * @FilePath: /utilization-platform/src/pages/VisualizationAnalysis/DeviceStatus/components/header/index.tsx
 * @Description: 设备状态头部tab
 */
import Icon from '@/components/SvgIcon';
import { Tooltip } from 'antd';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import style from '../../deviceStatus.module.scss';
import { TabType } from '../../type';
import ModalTable from '../modal-table';

const StatusHeader: React.FC<{ tabItem: TabType[] }> = ({ tabItem }) => {
  const navigate = useNavigate();

  const [tabType, setTabType] = useState<number>(-1);
  const [open, setOpen] = useState<boolean>(false);

  const handleTab = (item: TabType) => {
    setTabType(item.id);
    if (item.id === 1) {
      navigate('/systemConfig');
    } else if (item.num && item.num <= 0 && item.id === 4) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };
  return (
    <div className={style.header}>
      {tabItem.map(item => {
        return (
          <div
            className={style.headerItem}
            key={`device-status-${item.id}`}
            style={{
              backgroundColor: item.bgColor,
            }}
            onClick={() => handleTab(item)}>
            <div className={style.itemText}>
              <div
                className={style.itemTextTop}
                style={{
                  color: item.textColor,
                }}>
                {item.text}
                {item.tip && (
                  <Tooltip title={item.tip}>
                    <Icon name="icon-info2" className={style.IconStyle} />
                  </Tooltip>
                )}
              </div>
              <div className={style.itemTextNum}>
                {item.num}
                {item.id === 2 ? '类' : '台'}
                {item.percent && (
                  <span className={style.itemTextPercent}>
                    ( {item.percent})
                  </span>
                )}
              </div>
            </div>
            <img src={item.img} className={style.itemImg} />
          </div>
        );
      })}
      <ModalTable type={tabType} open={open} onCancel={() => setOpen(false)} />
    </div>
  );
};
export default StatusHeader;
