/*
 * @Author: qingyuanyang
 * @Date: 2023-09-19 11:05:18
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-11 14:54:01
 * @FilePath: /utilization-platform/src/pages/VisualizationAnalysis/DeviceStatus/components/modal-table/index.tsx
 * @Description: 弹窗列表
 */

import { useEffect, useState } from 'react';
import { ModalProps } from 'antd/lib';
import {
  getDeviceDeptCount,
  getDeviceMonthlyIncrCount,
  getDeviceTypeStatistics,
} from '@/api/deviceStatus';
import { useRequest } from 'ahooks';
import { DeviceTabType } from '../../type';
import { ColumnsType } from 'antd/es/table';
import Modal from '@/components/modal';
import style from './index.module.scss';
import React from 'react';
import Table from '@/components/table';

interface Props extends ModalProps {
  type: number;
}

const ModalTable: React.FC<Props> = ({ type, ...rest }) => {
  const [data, setData] = useState([]);
  const [columns, setColumns] = useState<ColumnsType<DeviceTabType>>();
  const [title, setTitle] = useState<string>('设备类型');
  const defaultColumns: ColumnsType<DeviceTabType> = [
    {
      title: '序号',
      dataIndex: 'id',
      key: 'id',
      width: 56,
      render: (_, __, index) => {
        return <span>{index + 1}</span>;
      },
    },
  ];

  const countColumns: ColumnsType<DeviceTabType> = [
    {
      title: '设备数量',
      dataIndex: 'devCount',
      key: 'devCount',
      width: type === 4 ? 136 : 224,
    },
  ];
  const typeColumns = [
    {
      title: '设备类型',
      dataIndex: 'deviceType',
      key: 'deviceType',
      width: type === 4 ? 155 : 224,
    },
  ];
  const departColumns = [
    {
      title: '使用部门',
      dataIndex: 'department',
      key: 'department',
      width: type === 3 ? 224 : 155,
    },
  ];

  useEffect(() => {
    let arr = defaultColumns.concat([
      ...departColumns,
      ...typeColumns,
      ...countColumns,
    ]);
    switch (type) {
      case 4:
        getDeviceMonthlyData();
        setColumns(arr);
        setTitle('新增设备明细');
        break;

      case 3:
        arr = defaultColumns.concat([...departColumns, ...countColumns]);
        setColumns(arr);
        setTitle('使用部门');
        getDeviceDeptData();
        break;

      case 2:
        let arr3 = defaultColumns.concat([...typeColumns, ...countColumns]);
        setColumns(arr3);
        setTitle('设备类型');
        getDeviceTypeData();
        break;
    }
  }, [type]);

  const { run: getDeviceTypeData, loading: typeLoading } = useRequest(
    () => getDeviceTypeStatistics({ isDeviceExist: 1 }),
    {
      manual: true,
      onSuccess: ({ data }) => {
        setData(data);
      },
      onError: err => {
        console.log(err);
      },
    }
  );

  const { run: getDeviceMonthlyData, loading: monthLoading } = useRequest(
    () => getDeviceMonthlyIncrCount(),
    {
      manual: true,
      onSuccess: ({ data }) => {
        setData(data);
      },
      onError: err => {
        console.log(err);
      },
    }
  );

  const { run: getDeviceDeptData, loading: countLoading } = useRequest(
    () => getDeviceDeptCount(),
    {
      manual: true,
      onSuccess: ({ data }) => {
        setData(data);
      },
      onError: err => {
        console.log(err);
      },
    }
  );
  //当前页面ref
  const divRef = React.useRef<HTMLDivElement | null>(null);

  return (
    <div className={style.modalTable} ref={r => (divRef.current = r)}>
      <Modal
        {...rest}
        footer={false}
        title={title}
        width={560}
        getContainer={divRef.current || false}>
        <Table
          dataSource={data}
          pagination={false}
          columns={columns}
          size="small"
          scroll={{ y: 'calc(100vh - 300px)' }}
          loading={monthLoading || countLoading || typeLoading}
        />
      </Modal>
    </div>
  );
};

export default ModalTable;
