/*
 * @Author: yzr
 * @Date: 2023-09-18 10:34:59
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-09 16:19:04
 * @Description: 利用率分析
 * @FilePath: /utilization/src/pages/utiAnalysis/index.tsx
 */
import { Tabs } from 'antd';
import type { TabsProps } from 'antd';

import Static from './static';
import Detail from './detail';

import './index.scss';
import { useQuery } from '@/hooks/useQuery';

const UtiAnalysis: React.FC = () => {
  const code = useQuery('code');
  const items: TabsProps['items'] = [
    {
      key: '1',
      label: '利用率统计',
      children: <Static />,
    },
    {
      key: '2',
      label: '利用率详情',
      children: <Detail code={code} />,
    },
  ];

  return (
    <>
      <Tabs
        className="analysis"
        defaultActiveKey={code ? '2' : '1'}
        items={items}
      />
    </>
  );
};

export default UtiAnalysis;
