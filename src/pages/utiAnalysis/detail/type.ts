/*
 * @Author: yzr
 * @Date: 2023-09-22 10:22:24
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-22 15:51:56
 * @Description:
 * @FilePath: /utilization/src/pages/utiAnalysis/detail/type.ts
 */
export interface DataType {
  id: React.Key;
  statisticsTime: string;
  utilization: number;
  cumulativeWorkingHours: number;
  cumulativeWorkingNum: number;
  deviceCode?: string;
  lowThreshold?: number; // 低阈值
  highThreshold?: number; // 高阈值
}
