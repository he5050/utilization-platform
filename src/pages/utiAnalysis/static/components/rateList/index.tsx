/*
 * @Author: yzr
 * @Date: 2023-09-18 14:22:54
 * @LastEditors: yzr
 * @LastEditTime: 2023-10-11 16:53:02
 * @Description:
 * @FilePath: /utilization/src/pages/utiAnalysis/static/components/rateList/index.tsx
 */
import { useState } from 'react';
import { Space, Button, Select } from 'antd';
import Card from '@/components/Card';
import { useRequest } from 'ahooks';
import { getDepList } from '@/api/device';
import { getLowRate, getHighRate } from '@/api/analysis';

import type { ColumnType } from 'antd/es/table';

import style from './index.module.scss';
import Table from '@/components/table';
import Ellipsis from '@/components/ellipsis-text';

interface selectType {
  label: any;
  value: any;
}

interface Iprops {
  deviceTypeOptions: any;
}

const RateList: React.FC<Iprops> = ({ deviceTypeOptions }) => {
  const [deptOptions, setDeptTypeOptions] = useState<selectType[]>();
  const [lowParams, setLowParams] = useState<any>({
    deviceType: [],
    department: [],
  });
  const [highParams, setHighParams] = useState<any>({
    deviceType: [],
    department: [],
  });

  // 获取部门列表
  useRequest(() => getDepList(), {
    onSuccess: res => {
      const options = res.data.map(r => ({
        label: r,
        value: r,
      }));
      setDeptTypeOptions(options);
    },
  });

  const {
    data: highRateData,
    run: highRateRun,
    loading: highLoading,
  } = useRequest(() => getHighRate(highParams));

  const {
    data: lowRateData,
    run: lowRateRun,
    loading: lowLoading,
  } = useRequest(() => getLowRate(lowParams));

  const getColumnSearchProps = (
    dataIndex: string,
    paramsType: 'low' | 'high'
  ): ColumnType<any> => ({
    filterDropdown: ({ setSelectedKeys, close }) => (
      <div style={{ padding: 8 }} onKeyDown={e => e.stopPropagation()}>
        <Select
          mode="multiple"
          allowClear
          placeholder="请选择"
          onChange={val => {
            setSelectedKeys(val);
            if (paramsType === 'low') {
              setLowParams({
                ...lowParams,
                [dataIndex]: val,
              });
            } else {
              setHighParams({
                ...highParams,
                [dataIndex]: val,
              });
            }
          }}
          style={{ marginBottom: 8, display: 'block' }}
          maxTagCount={2}
          options={dataIndex === 'department' ? deptOptions : deviceTypeOptions}
        />
        <Space style={{ display: 'flex', justifyContent: 'end' }}>
          <Button
            type="primary"
            size="small"
            style={{ width: 90 }}
            onClick={() => {
              paramsType === 'low' ? lowRateRun() : highRateRun();
              close();
            }}>
            查询
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              setSelectedKeys([]);
              close();
            }}>
            取消
          </Button>
        </Space>
      </div>
    ),
  });

  const highColumns = [
    {
      title: '设备编号',
      dataIndex: 'deviceCode',
      key: 'deviceCode',
      width: 120,
      render: (value: any) => <Ellipsis content={value} />,
    },
    {
      title: '设备类型',
      dataIndex: 'deviceType',
      key: 'deviceType',
      width: 100,
      render: (value: any) => <Ellipsis content={value} />,
      ...getColumnSearchProps('deviceType', 'high'),
    },
    {
      title: '利用率高阈值',
      dataIndex: 'highUtilizationThreshold',
      key: 'highUtilizationThreshold',
      render: (text: number) => {
        return <span>{(text * 100).toFixed(0)}%</span>;
      },
    },
    {
      title: '部门',
      dataIndex: 'department',
      key: 'department',
      width: 90,
      render: (value: any) => <Ellipsis content={value} />,
      ...getColumnSearchProps('department', 'high'),
    },
    {
      title: '型号',
      dataIndex: 'deviceModel',
      key: 'deviceModel',
      render: (value: any) => <Ellipsis content={value} />,
    },
    {
      title: '利用率',
      dataIndex: 'utilizationRate',
      key: 'utilizationRate',
      render: (text: number) => {
        return <span>{(text * 100).toFixed(0)}%</span>;
      },
    },
  ];

  const lowColumns = [
    {
      title: '设备编号',
      dataIndex: 'deviceCode',
      key: 'deviceCode',
      width: 120,
      render: (value: any) => <Ellipsis content={value} />,
    },
    {
      title: '设备类型',
      dataIndex: 'deviceType',
      key: 'deviceType',
      width: 100,
      render: (value: any) => <Ellipsis content={value} />,
      ...getColumnSearchProps('deviceType', 'low'),
    },
    {
      title: '利用率低阈值',
      dataIndex: 'lowUtilizationThreshold',
      key: 'lowUtilizationThreshold',
      render: (text: number) => {
        return <span>{(text * 100).toFixed(0)}%</span>;
      },
    },
    {
      title: '部门',
      dataIndex: 'department',
      key: 'department',
      width: 90,
      render: (value: any) => <Ellipsis content={value} />,
      ...getColumnSearchProps('department', 'low'),
    },
    {
      title: '型号',
      dataIndex: 'deviceModel',
      key: 'deviceModel',
      render: (value: any) => <Ellipsis content={value} />,
    },
    {
      title: '利用率',
      dataIndex: 'utilizationRate',
      key: 'utilizationRate',
      render: (text: number) => {
        return <span>{(text * 100).toFixed(0)}%</span>;
      },
    },
  ];

  return (
    <div className={style.rateList}>
      <div className={style.topList}>
        <Card title="上月利用率最高设备" tips="利用率超过设置的利用率高阈值">
          <Table
            scroll={{ y: 320 }}
            loading={highLoading}
            dataSource={highRateData?.data}
            columns={highColumns}
            pagination={false}
          />
        </Card>
        <Card title="上月利用率最低设备" tips="利用率低于设置的利用率低阈值">
          <Table
            scroll={{ y: 320 }}
            loading={lowLoading}
            dataSource={lowRateData?.data}
            columns={lowColumns}
            pagination={false}
          />
        </Card>
      </div>
    </div>
  );
};

export default RateList;
