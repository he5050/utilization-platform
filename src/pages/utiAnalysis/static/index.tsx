/*
 * @Author: yzr
 * @Date: 2023-09-18 10:56:55
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-26 16:23:31
 * @Description: 利用率统计
 * @FilePath: /utilization/src/pages/utiAnalysis/static/index.tsx
 */
import { useState } from 'react';
import { Col, Row, Image } from 'antd';
import RateList from './components/rateList';
import RateChat from './components/rateChat';

import { useRequest } from 'ahooks';
import { getUtilizationRate } from '@/api/analysis';
import { getDeviceTypeList } from '@/api/device';

import averageImg from '@/assets/image/cardIcon1.png';
import maxImg from '@/assets/image/cardIcon3.png';
import minImg from '@/assets/image/cardIcon2.png';
import style from './index.module.scss';

const Static: React.FC = () => {
  const { data: utilRateData } = useRequest(() => getUtilizationRate(), {
    manual: false,
  });

  // 设备类型下拉
  const [deviceTypeOptions, setDeviceOptions] = useState<
    {
      label: any;
      value: any;
    }[]
  >();

  // 获取设备类型
  useRequest(() => getDeviceTypeList({ page: 1, pageSize: 9999 }), {
    onSuccess: res => {
      const deviceTypeList = res.data.records?.map(r => ({
        label: r.deviceType,
        value: r.deviceType,
      }));
      setDeviceOptions(deviceTypeList);
    },
  });

  return (
    <div className={style.static}>
      <div className={style.tips}>
        <Row gutter={16} className={style.listRow}>
          <Col className="gutter-row" span={8}>
            <div
              className={style.rateCard}
              style={{ backgroundColor: '#e6f0ff' }}>
              <div className={style.leftCard}>
                <div style={{ color: '#3471FF' }}>本月平均利用率</div>
                <div className={style.weightFont}>
                  {(
                    Number(utilRateData?.data.averageUtilizationRate) * 100
                  ).toFixed(0)}
                  %
                </div>
              </div>
              <div className={style.imgCard}>
                <Image src={averageImg} preview={false} />
              </div>
            </div>
          </Col>
          <Col className="gutter-row" span={8}>
            <div
              className={style.rateCard}
              style={{ backgroundColor: '#e2f7f1' }}>
              <div className={style.leftCard}>
                <div style={{ color: '#0DC191' }}>本月利用率最高部门</div>
                <div className={style.weightFont}>
                  {utilRateData?.data.maximumDepartmentUtilizationRate
                    ? `${
                        utilRateData?.data.maximumDepartmentUtilizationRate
                          .department
                      } ${(
                        Number(
                          utilRateData?.data.maximumDepartmentUtilizationRate
                            .utilizationRate
                        ) * 100
                      ).toFixed(0)}%`
                    : '-'}
                </div>
              </div>
              <div className={style.imgCard}>
                <Image src={maxImg} preview={false} />
              </div>
            </div>
          </Col>
          <Col className="gutter-row" span={8}>
            <div
              className={style.rateCard}
              style={{ backgroundColor: '#e6e4fe' }}>
              <div className={style.leftCard}>
                <div style={{ color: '#706CFA' }}>本月利用率最低部门</div>
                <div className={style.weightFont}>
                  {utilRateData?.data.minimumDepartmentUtilizationRate
                    ? `${
                        utilRateData?.data.minimumDepartmentUtilizationRate
                          .department
                      } ${(
                        Number(
                          utilRateData?.data.minimumDepartmentUtilizationRate
                            .utilizationRate
                        ) * 100
                      ).toFixed(0)}%`
                    : '-'}
                </div>
              </div>
              <div className={style.imgCard}>
                <Image src={minImg} preview={false} />
              </div>
            </div>
          </Col>
        </Row>
        <p className={style['tips-desc']}>
          利用率统计结果由定时任务每日0时计算后得出，统计结果截止至昨日，利用率=工作时长/利用率计算基数*100%
        </p>
      </div>

      <RateList deviceTypeOptions={deviceTypeOptions} />
      <RateChat deviceTypeOptions={deviceTypeOptions} />
    </div>
  );
};

export default Static;
