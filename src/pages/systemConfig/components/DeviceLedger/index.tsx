import { useState, useEffect, useRef } from 'react';
import {
  Form,
  Input,
  Select,
  Button,
  // Table,
  Modal,
  Checkbox,
  Row,
  Col,
  Upload,
  message,
  Tag,
  Dropdown,
  Space,
  Divider,
} from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { ExclamationCircleFilled } from '@ant-design/icons';
import {
  DndContext,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  SortableContext,
  arrayMove,
  useSortable,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { restrictToVerticalAxis } from '@dnd-kit/modifiers';
import { CSS } from '@dnd-kit/utilities';
import { useRequest } from 'ahooks';
import classNames from 'classnames';
import dayjs from 'dayjs';

import type { FormInstance, UploadFile, MenuProps } from 'antd';
import type { Dispatch, SetStateAction } from 'react';
import type { ColumnsType } from 'antd/es/table';
import type { SelectProps } from 'antd';
import type { DragEndEvent } from '@dnd-kit/core';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import type { PaginationRef } from '@/components/PaginationSelf';

import Icon from '@/components/SvgIcon';
import PaginationSelf from '@/components/PaginationSelf';
import BeforefixSelect from '@/components/BeforefixSelect';

import { getDeviceTypeList, getDepList } from '@/api/device';
import {
  getLedgerTableAPI,
  addDeviceLedgerAPI,
  addMultiDeviceLedgerAPI,
  updateDeviceLedgerAPI,
  delDeviceLedgerAPI,
  exportDeviceLedgerAPI,
} from '@/api/visualiza';
import { downloadExcel } from '@/utils';
import { useAutoRequest } from '@/hooks/useAutoLoading';

import style from './deviceLedger.module.scss';
import Table from '@/components/table';
import { useNavigate } from 'react-router-dom';
import useModal from '@/hooks/useModal';
import Ellipsis from '@/components/ellipsis-text';

const { Dragger } = Upload;
const workStatusOptions = [
  {
    label: '工作',
    value: 20,
  },
  {
    label: '待机',
    value: 10,
  },
  {
    label: '关机',
    value: 0,
  },
  {
    label: '离线',
    value: -10,
  },
];

const EditFormCom = ({
  name,
  form,
  deviceTypeOptions,
}: {
  name: string;
  form: FormInstance;
  deviceTypeOptions: SelectProps['options'];
}) => {
  return (
    <Form
      name={name}
      form={form}
      labelCol={{ span: 4 }}
      initialValues={{}}
      className={style['edit-form']}
      layout="horizontal"
      autoComplete="off">
      <Form.Item
        label="设备编号"
        name="deviceCode"
        rules={[{ required: true, message: '请输入设备编号' }]}>
        <Input disabled={name === 'editForm'} placeholder="请输入设备编号" />
      </Form.Item>
      <Form.Item
        label="设备类型"
        name="type"
        rules={[{ required: true, message: '请选择设备类型' }]}>
        <Select
          placeholder="请选择设备类型"
          disabled={name === 'editForm'}
          options={deviceTypeOptions}></Select>
      </Form.Item>
      <Form.Item
        label="所属部门"
        name="department"
        rules={[{ required: true, message: '请输入所属部门' }]}>
        <Input placeholder="请输入所属部门" />
      </Form.Item>
      <Form.Item
        label="设备型号"
        name="model"
        rules={[{ required: true, message: '请输入设备型号' }]}>
        <Input placeholder="请输入设备型号" />
      </Form.Item>
      <Form.Item label="设备厂家" name="manufacturer">
        <Input placeholder="请输入设备厂家" />
      </Form.Item>
      <Form.Item label="负责人" name="principal">
        <Input placeholder="请输入负责人" />
      </Form.Item>
    </Form>
  );
};

const MultipleCom = ({
  isModalOpen,
  setFileUpName,
  form,
}: // deviceTypeOptions,
{
  isModalOpen: boolean;
  setFileUpName: Dispatch<SetStateAction<Blob | undefined>>;
  form: FormInstance;
  deviceTypeOptions: SelectProps['options'];
}) => {
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const downloadTemplate = () => {
    window.location.href =
      'https://pharmaoryx-iot-1310692535.cos.ap-chongqing.myqcloud.com/euvs/%E6%A8%A1%E7%89%88/EUVS%E8%AE%BE%E5%A4%87%E5%AF%BC%E5%85%A5%E6%A8%A1%E7%89%88.xlsx ';
  };
  useEffect(() => {
    if (!isModalOpen) {
      setFileList([]);
    }
  }, [isModalOpen]);
  return (
    <>
      <div className={style.addInfo}>
        <Icon
          name="icon-info"
          style={{ color: '#4E6FF5', marginRight: '8px' }}
        />
        您可下载「标准模板」，填写信息后上传
        <span className={style.downLoad} onClick={downloadTemplate}>
          {'立即下载'}
        </span>
      </div>
      <Form
        name="uploadForm"
        form={form}
        layout="vertical"
        style={{ padding: '0 24px' }}>
        <Form.Item
          name="fileUpName"
          rules={[{ required: true, message: '请选择上传的excel文件' }]}>
          <Dragger
            name="fileUpName"
            accept=".xls, .xlsx"
            maxCount={1}
            // onChange={event => {
            //   console.log(event);
            // setFileList([event.file]);
            // }}
            onRemove={file => {
              console.log(fileList.filter(item => item !== file));
              setFileList(fileList.filter(item => item !== file));
            }}
            // fileList={fileList}
            customRequest={option => {
              try {
                setFileUpName(option.file as File);
                option.onSuccess!(option.file);
              } catch (error) {
                option.onError!(error as any);
              }
            }}>
            <Button
              icon={
                <Icon
                  name="icon-a-02Icontubiao_mianxing_shangchuan"
                  style={{ fontSize: '36px' }}
                />
              }
              className={style.uploadBtn}
            />
            <p style={{ color: '#4E6FF5' }}>选择文件</p>
          </Dragger>
        </Form.Item>
      </Form>
    </>
  );
};
const columnList = [
  {
    label: '厂家',
    value: 'manufacturer',
  },
  {
    label: '负责人',
    value: 'principal',
  },
];

interface ColParams {
  value: string;
  label: string;
}
const ColCom = ({ colAttrs }: { colAttrs: ColParams }) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id: colAttrs.value,
  });
  const style2: React.CSSProperties = {
    transform: CSS.Transform.toString(transform && { ...transform, scaleY: 1 }),
    transition,
    cursor: 'move',
    ...(isDragging ? { position: 'relative', zIndex: 9999 } : {}),
  };
  return (
    <Col
      className={style.colColumn}
      span={24}
      ref={setNodeRef}
      style={style2}
      {...attributes}
      {...listeners}>
      <Checkbox value={colAttrs.value}>{colAttrs.label}</Checkbox>
    </Col>
  );
};
const FilterContent: React.FC<{
  setOpen: Dispatch<SetStateAction<boolean>>;
  setColumns: (P: ColParams[]) => void;
}> = ({ setOpen, setColumns }) => {
  const [tablesColumn, setTablesColumn] = useState(columnList);
  const [checkedColumn, setCheckedColumn] = useState<CheckboxValueType[]>([]);
  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: {
        distance: 1,
      },
    })
  );

  const onDragEnd = ({ active, over }: DragEndEvent) => {
    if (active.id !== over?.id) {
      setTablesColumn(prev => {
        const activeIndex = prev.findIndex(i => i.value === active.id);
        const overIndex = prev.findIndex(i => i.value === over?.id);
        return arrayMove(prev, activeIndex, overIndex);
      });
    }
  };
  const checkedChangeHandle = (checkedValues: CheckboxValueType[]) => {
    setCheckedColumn(checkedValues);
  };
  return (
    <div className={style.filterColumn}>
      <DndContext
        sensors={sensors}
        modifiers={[restrictToVerticalAxis]}
        onDragEnd={onDragEnd}>
        <SortableContext
          // rowKey array
          items={tablesColumn.map(i => i.value)}
          strategy={verticalListSortingStrategy}>
          <Checkbox.Group value={checkedColumn} onChange={checkedChangeHandle}>
            <Row>
              {tablesColumn.map(item => {
                return <ColCom key={item.value} colAttrs={item}></ColCom>;
              })}
            </Row>
          </Checkbox.Group>
        </SortableContext>
      </DndContext>
      <Space style={{ width: '100%', justifyContent: 'end' }}>
        <Button
          size="small"
          onClick={() => {
            setOpen(false);
            setTablesColumn(columnList);
          }}>
          取消
        </Button>
        <Button
          type="primary"
          size="small"
          onClick={() => {
            setOpen(false);
            const columnsValue = tablesColumn.filter(item =>
              checkedColumn.includes(item.value)
            );
            setColumns(columnsValue);
          }}>
          确定
        </Button>
      </Space>
    </div>
  );
};
const DeviceLedger: React.FC = () => {
  const [modal, contextHolder] = useModal();
  const navigate = useNavigate();
  const [isFilterDropdownOpen, setIsFilterDropdownOpen] =
    useState<boolean>(false);
  const defaultColumns: ColumnsType<API.LedgerTable & { id: string }> = [
    {
      title: '设备编号',
      dataIndex: 'deviceCode',
      key: 'deviceCode',
    },
    {
      title: '状态',
      dataIndex: 'workStatusString',
      width: 100,
      key: 'workStatusString',
      render: (workStatusString, recode) => {
        let color = 'closed';
        switch (recode.workStatus) {
          case 0:
            color = 'closed';
            break;
          case 10:
            color = 'waiting';
            break;
          case 20:
            color = 'working';
            break;
          case -10:
            color = 'offline';
            break;
          default:
            break;
        }
        return (
          <Tag style={{ border: 'none' }} className={style[color]}>
            {workStatusString}
          </Tag>
        );
      },
    },
    {
      title: '设备类型',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: '所属部门',
      dataIndex: 'department',
      key: 'department',
      render: value => <Ellipsis content={value} />,
    },
    {
      title: '设备型号',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: '录入时间',
      dataIndex: 'entryTime',
      key: 'entryTime',
      render: value => {
        return <span>{dayjs(value).format('YYYY/MM/DD')}</span>;
      },
    },
  ];
  const operateRow: ColumnsType<API.LedgerTable & { id: string }> = [
    {
      title: '操作',
      dataIndex: 'operate',
      key: 'operate',
      width: 160,
      // fixed: 'right',
      filterDropdownOpen: isFilterDropdownOpen,
      onFilterDropdownOpenChange: setIsFilterDropdownOpen,
      filterDropdown: (
        <FilterContent
          setOpen={setIsFilterDropdownOpen}
          setColumns={setResultColumns}
        />
      ),
      filterIcon: () => (
        <Icon name="icon-basic_arrangement_nor" style={{ color: '#505159' }} />
      ),
      render: (_, recode) => {
        return (
          <>
            <Button
              type="link"
              style={{ padding: 0, height: 'auto', color: '#4E6FF5' }}
              onClick={() => {
                setIsEditModalOpen(true);
                editForm.setFieldsValue(recode);
                setId(recode.id);
              }}>
              编辑
            </Button>
            <Divider
              type="vertical"
              style={{ height: 14, borderInlineStartColor: '#DCDEE0' }}
            />
            <Button
              type="link"
              style={{ padding: 0, height: 'auto', color: '#4E6FF5' }}
              onClick={() => {
                modal.warning({
                  title: '确认要删除该设备吗？',
                  centered: true,
                  icon: (
                    <ExclamationCircleFilled
                      style={{ color: '#E34D59', fontSize: '16px' }}
                    />
                  ),
                  content: (
                    <span style={{ color: '#4E5969' }}>
                      删除设备【{recode.deviceCode}】后，将无法恢复
                    </span>
                  ),
                  cancelText: ' 取消 ', // 文字前后空格是为了取消antd自动在文字中间插入的空格
                  okText: ' 删除 ',
                  okType: 'danger',
                  onOk() {
                    delDeviceLedgerAPI({ id: recode.id }).then(() => {
                      message.success('删除成功');
                      paginationRef.current?.getPageList();
                    });
                  },
                });
              }}>
              删除
            </Button>
            <Divider
              type="vertical"
              style={{ height: 14, borderInlineStartColor: '#DCDEE0' }}
            />
            <Button
              type="link"
              style={{ padding: 0, height: 'auto', color: '#4E6FF5' }}
              onClick={() => {
                navigate(`/utiAnalysis?code=${recode.deviceCode}`);
              }}>
              详情
            </Button>
            {contextHolder}
          </>
        );
      },
    },
  ];
  const [deviceTypeOptions, setDeviceTypeOptions] =
    useState<SelectProps['options']>();
  const [deptOptions, setDeptOptions] = useState<SelectProps['options']>();
  const [formData, setFormData] = useState<API.AnalysisConfigList>({}); // 搜索表单数据
  const [dataSource, setDataSource] = useState<
    (API.LedgerTable & { id: string })[]
  >([]);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false);
  const [currentTab, setCurrentTab] = useState<string>('single');
  const [fileUpName, setFileUpName] = useState<Blob>();
  const [id, setId] = useState<string>();
  const [visibleColumns, setVisibleColumns] = useState<
    ColumnsType<API.LedgerTable & { id: string }>
  >([...defaultColumns]);

  const [tableLoading, getLedgerTableMapAPI] =
    useAutoRequest(getLedgerTableAPI);

  const [filterForm] = Form.useForm();
  const [addForm] = Form.useForm();
  const [uploadForm] = Form.useForm();
  const [editForm] = Form.useForm();

  const paginationRef = useRef<PaginationRef>(null);

  // 获取设备类型
  useRequest(() => getDeviceTypeList({ page: 1, pageSize: 9999 }), {
    onSuccess: res => {
      const deviceTypeList = res.data.records?.map(r => ({
        label: r.deviceType,
        value: r.deviceType,
      }));
      setDeviceTypeOptions(deviceTypeList);
    },
  });

  useRequest(() => getDepList(), {
    onSuccess: res => {
      const options = res.data.map(r => ({
        label: r,
        value: r,
      }));
      setDeptOptions(options);
    },
  });

  const items = [
    {
      label: '新建设备',
      key: 'single',
    },
    {
      label: '批量添加设备',
      key: 'multiple',
    },
  ];

  function setResultColumns(moreColumn: ColParams[]) {
    let result = moreColumn.map(item => {
      return {
        title: item.label,
        dataIndex: item.value,
        key: item.value,
      };
    });
    setVisibleColumns([...defaultColumns, ...result]);
  }
  const resetFrom = (form: FormInstance) => {
    form.resetFields();
    setFormData({});
  };
  const setTableData = (data: any) => {
    setDataSource(data?.records);
  };
  const closeAddModal = () => {
    setIsModalOpen(false);
    addForm.resetFields();
    uploadForm.resetFields();
    // setCurrentTab('single');
  };
  const closeEditModal = () => {
    setIsEditModalOpen(false);
  };

  const addSuccessFn = () => {
    message.success('新增成功');
    closeAddModal();
    paginationRef.current?.getPageList();
  };

  // 新增
  const saveDeviceHandle = () => {
    if (currentTab === 'single') {
      addForm.validateFields().then(
        () => {
          let data = addForm.getFieldsValue();
          addDeviceLedgerAPI(data).then(res => {
            if (res?.success) addSuccessFn();
          });
        },
        () => {}
      );
    } else {
      uploadForm.validateFields().then(
        () => {
          console.log(fileUpName, fileUpName as Blob);
          addMultiDeviceLedgerAPI(fileUpName).then(res => {
            if (res) addSuccessFn();
          });
        },
        () => {}
      );
    }
  };
  // 修改
  const updateDeviceHandle = () => {
    editForm.validateFields({ validateOnly: true }).then(
      () => {
        updateDeviceLedgerAPI({ id, ...editForm.getFieldsValue() }).then(() => {
          message.success('编辑成功');
          setIsEditModalOpen(false);
          paginationRef.current?.getPageList();
        });
      },
      () => {}
    );
  };
  const exportFilterResult = () => {
    exportDeviceLedgerAPI(filterForm.getFieldsValue()).then(res => {
      downloadExcel(
        res.data,
        res.headers['content-disposition'].split('=')[1].split(';')[0]
      );
    });
  };
  const onClick: MenuProps['onClick'] = ({ key }) => {
    console.log(key);
    setCurrentTab(key);
    setIsModalOpen(true);
  };

  return (
    <div className={classNames(style.ledger, 'flex-column')}>
      <div className={classNames('flex-s-b')}>
        <div className={style.search}>
          <Form
            name="filterForm"
            form={filterForm}
            style={{ padding: '24px 0 12px' }}
            layout="inline"
            autoComplete="off">
            <Form.Item label="" name="searchKey">
              <Input
                placeholder="请输入关键字进行查询"
                allowClear
                prefix={
                  <Icon
                    name="icon-search"
                    style={{ fontSize: '14px', color: '#B1B5BD' }}
                  />
                }
              />
            </Form.Item>
            <Form.Item label="" name="departments">
              <BeforefixSelect
                style={{ minWidth: '120px' }}
                beforefix="部门"
                placeholder="请选择部门"
                allowClear
                mode="multiple"
                maxTagCount={2}
                options={deptOptions}
              />
            </Form.Item>
            <Form.Item label="" name="deviceTypes">
              <BeforefixSelect
                style={{ minWidth: '140px' }}
                beforefix="设备类型"
                placeholder="请选择设备类型"
                allowClear
                mode="multiple"
                maxTagCount={2}
                options={deviceTypeOptions}
              />
            </Form.Item>
            <Form.Item label="" name="workStatus">
              <BeforefixSelect
                style={{ minWidth: '140px' }}
                beforefix="设备状态"
                placeholder="请选择设备状态"
                allowClear
                mode="multiple"
                maxTagCount={2}
                options={workStatusOptions}
              />
            </Form.Item>
          </Form>
        </div>
        <Space>
          <Button
            htmlType="button"
            style={{ marginRight: '8px' }}
            onClick={() => resetFrom(filterForm)}>
            重置
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => setFormData(filterForm.getFieldsValue())}>
            查询
          </Button>
        </Space>
      </div>
      <Space style={{ marginBottom: '24px' }}>
        <Dropdown menu={{ items, onClick }} overlayClassName="dropStyle">
          <Button type="primary">
            <span>新增设备</span>
            <DownOutlined />
          </Button>
        </Dropdown>
        <Button type="primary" ghost onClick={exportFilterResult}>
          导出数据
        </Button>
      </Space>
      <Table
        columns={[...visibleColumns, ...operateRow]}
        rowKey="id"
        scroll={{ y: 'calc(100vh - 384px)' }}
        loading={tableLoading}
        pagination={false}
        dataSource={dataSource}
      />
      <PaginationSelf
        ref={paginationRef}
        api={getLedgerTableMapAPI}
        formData={formData}
        setTableData={setTableData}
      />
      {/* 新增 modal */}
      <Modal
        forceRender
        title={currentTab === 'single' ? '新增设备' : '批量添加设备'}
        centered
        getContainer={false}
        open={isModalOpen}
        onOk={saveDeviceHandle}
        okText="保存"
        onCancel={closeAddModal}
        width={560}>
        {currentTab === 'single' ? (
          <EditFormCom
            name="addForm"
            form={addForm}
            deviceTypeOptions={deviceTypeOptions}
          />
        ) : (
          <MultipleCom
            isModalOpen={isModalOpen}
            setFileUpName={setFileUpName}
            form={uploadForm}
            deviceTypeOptions={deviceTypeOptions}
          />
        )}
      </Modal>
      {/* 编辑 modal */}
      <Modal
        forceRender
        title="编辑设备"
        centered
        getContainer={false}
        open={isEditModalOpen}
        onOk={updateDeviceHandle}
        okText="保存"
        onCancel={closeEditModal}
        width={560}>
        <EditFormCom
          name="editForm"
          form={editForm}
          deviceTypeOptions={deviceTypeOptions}
        />
      </Modal>
      {/* 删除modal */}
    </div>
  );
};

export default DeviceLedger;
