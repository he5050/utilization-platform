/*
 * @Author: yzr
 * @Date: 2023-09-15 15:30:17
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-12 14:21:21
 * @Description:
 * @FilePath: /utilization/src/pages/systemConfig/index.tsx
 */
import { Tabs } from 'antd';
import type { TabsProps } from 'antd';

import { useLocation } from 'react-router-dom';

import DeviceLedger from './components/DeviceLedger';
import InstrumentSetting from './components/InstrumentSetting';
import AnalysisConfig from './components/AnalysisConfig';
import './index.scss';
import { useEffect, useState } from 'react';

const SystemConfig: React.FC = () => {
  const items: TabsProps['items'] = [
    {
      key: '1',
      label: '设备台账',
      children: <DeviceLedger />,
    },
    {
      key: '2',
      label: '采集配置',
      children: <AnalysisConfig />,
    },
    {
      key: '3',
      label: '仪器设置',
      children: <InstrumentSetting />,
    },
  ];

  const [activeKey, setActiveKey] = useState<string>();

  const location = useLocation();
  console.log('useLocation', location);

  useEffect(() => {
    if (location.search) {
      const tab = location.search.replace('?', '');
      console.log(tab);
      setActiveKey(tab);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Tabs
        activeKey={activeKey}
        defaultActiveKey="1"
        items={items}
        className="system"
        onChange={key => setActiveKey(key)}
        destroyInactiveTabPane
      />
    </>
  );
};

export default SystemConfig;
