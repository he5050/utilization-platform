/*
 * @Author: qingyuanyang
 * @Date: 2023-10-08 14:04:02
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-09 16:46:59
 * @FilePath: /utilization-platform/src/utils/echartConfig.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import NoData from '@/assets/image/empty-chart.png';

export const graphic = [
  {
    type: 'group',
    left: 'center',
    top: 'middle',
    children: [
      {
        type: 'image',
        left: -50,
        top: -54,
        style: {
          image: NoData,
          width: 74,
          height: 50,
        },
      },
      {
        type: 'text',
        left: -38,
        style: {
          text: '暂无数据',
          textAlign: 'center',
          font: '1em sans-serif',
          opacity: 0.5,
        },
      },
    ],
  },
];
