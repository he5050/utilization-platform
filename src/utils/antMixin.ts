import dayjs, { Dayjs } from 'dayjs';
import type { ManipulateType } from 'dayjs';

//不包含当日
export function disabledDate(current: Dayjs) {
  if (current) {
    return (
      current &&
      (current < dayjs().subtract(1, 'day').subtract(1, 'year') ||
        current > dayjs().subtract(1, 'day').endOf('day'))
    );
  }
  return false;
}
//禁止选择未来日期
export function disabledDateHasCurrent(current: Dayjs) {
  if (current) {
    return current && current > dayjs().endOf('day');
  }
  return false;
}

export function disabledCommonDate(
  dateType: ManipulateType = 'day',
  timeSpan: number,
  subtract: number = 1
) {
  return function (current: Dayjs) {
    if (current) {
      return (
        current &&
        (current <
          dayjs().subtract(subtract, dateType).subtract(timeSpan, dateType) ||
          current > dayjs().subtract(subtract, dateType).endOf(dateType))
      );
    }
    return false;
  };
}
