import { Modal, theme } from 'antd';
import type { ModalFuncProps } from 'antd';
import type { ModalFuncWithPromise } from 'antd/es/modal/useModal';
import { useMemo } from 'react';
import Icon from '@/components/SvgIcon';
import style from './index.module.scss';

export enum SizeType {
  SMALL = 'small',
  DEFAULT = 'default',
}

type HookAPI = {
  success: ModalFuncWithPromise;
  warning: ModalFuncWithPromise;
  error: ModalFuncWithPromise;
  info: ModalFuncWithPromise;
};

export default function useModal(
  size: SizeType | number = SizeType.DEFAULT
): [instance: HookAPI, contextHolder: React.ReactElement] {
  const [modal, contextHolder] = Modal.useModal();
  const {
    token: { colorInfo, colorSuccess, colorWarning, colorError },
  } = theme.useToken();

  const width = useMemo<number>(() => {
    switch (size) {
      case SizeType.DEFAULT:
        return 360;
      case SizeType.SMALL:
        return 214;
      default:
        return size;
    }
  }, [size]);

  const info = ({
    okButtonProps,
    cancelButtonProps,
    ...rest
  }: ModalFuncProps) => {
    return modal.confirm({
      width,
      centered: true,
      icon: (
        <Icon
          name="icon-a-02Icontubiao_mianxing_jinggao"
          style={{ color: colorInfo, width: 20, height: 20, fontSize: 18 }}
        />
      ),
      okButtonProps: {
        size: 'middle',
        ...okButtonProps,
      },
      cancelButtonProps: {
        size: 'middle',
        ...cancelButtonProps,
      },
      ...rest,
    });
  };

  const success = ({
    okButtonProps,
    cancelButtonProps,
    ...rest
  }: ModalFuncProps) => {
    return modal.confirm({
      width,
      centered: true,
      icon: (
        <Icon
          name="icon-a-02Icontubiao_mianxing_jinggao"
          style={{ color: colorSuccess, width: 20, height: 20, fontSize: 18 }}
        />
      ),
      okButtonProps: {
        size: 'middle',
        ...okButtonProps,
      },
      cancelButtonProps: {
        size: 'middle',
        ...cancelButtonProps,
      },
      ...rest,
    });
  };

  const warning = ({
    okButtonProps,
    cancelButtonProps,
    ...rest
  }: ModalFuncProps) => {
    return modal.confirm({
      width,
      centered: true,
      className: style.waringDiv,
      icon: (
        <Icon
          name="icon-a-02Icontubiao_mianxing_jinggao"
          style={{ color: colorWarning, width: 20, height: 20, fontSize: 18 }}
        />
      ),
      okButtonProps: {
        size: 'middle',
        ...okButtonProps,
      },
      cancelButtonProps: {
        size: 'middle',
        ...cancelButtonProps,
      },
      ...rest,
    });
  };

  const error = ({
    okButtonProps,
    cancelButtonProps,
    ...rest
  }: ModalFuncProps) => {
    return modal.confirm({
      width,
      centered: true,
      icon: (
        <Icon
          name="icon-a-02Icontubiao_mianxing_jinggao"
          style={{ color: colorError, width: 20, height: 20, fontSize: 18 }}
        />
      ),
      okButtonProps: {
        size: 'middle',
        danger: true,
        type: size === SizeType.SMALL ? 'default' : 'primary',
        ...okButtonProps,
      },
      cancelButtonProps: {
        size: 'middle',
        ...cancelButtonProps,
      },
      ...rest,
    });
  };

  const instance = {
    info,
    success,
    warning,
    error,
  };

  return [instance, contextHolder];
}
