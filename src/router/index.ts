import { createHashRouter } from 'react-router-dom';
import type { RouteObject } from 'react-router-dom';
import statisticAnalysis from './routes/visualizationAnalysis';

export const routes = [...statisticAnalysis];

const router = createHashRouter(routes as RouteObject[]);

export default router;
