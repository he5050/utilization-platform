import React, { Suspense } from 'react';
import { Spin } from 'antd';

const lazyComp = (Comp: React.LazyExoticComponent<any>): React.ReactNode => {
  return (
    <Suspense fallback={<Spin size="small" />}>
      <Comp />
    </Suspense>
  );
};

export default lazyComp;
