import type { RouteObject } from 'react-router-dom';

export declare interface RouteMeta
  extends Record<string | number | symbol, any> {}

export type RouteMapType = RouteObject & {
  meta?: RouteMeta;
};
export type RouteType = Omit<RouteMapType, 'children'> & {
  children?: RouteType[];
};
