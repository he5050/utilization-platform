/*
 * @Author: yzr
 * @Date: 2023-09-27 14:21:03
 * @LastEditors: yzr
 * @LastEditTime: 2023-10-08 17:59:28
 * @Description:
 * @FilePath: /utilization/src/router/routes/visualizationAnalysis.tsx
 */
import { lazy } from 'react';
import type { RouteObject } from 'react-router-dom';
import lazyLoadComp from '../lazyComp';
import Icon from '@/components/SvgIcon';

const Layouts = lazy(() => import('@/layouts'));

/**
 * path 将作为menu的唯一标识key
 * menuKey 当路由不在菜单栏中时，指定此时菜单选中的（高亮）key
 * openKey 用于指定默认展开菜单
 * parent 用于面包屑展示
 * handle key 仅当path为 '/' 时使用，替代path作为menu的唯一标识
 */
const statisticAnalysis: RouteObject[] = [
  {
    path: '/login',
    element: lazyLoadComp(lazy(() => import('@/pages/login'))),
    handle: {
      hide: true,
      title: '登录',
    },
  },
  {
    path: '/',
    element: lazyLoadComp(Layouts),
    handle: {
      // title: '可视化分析',
      key: 'euvs',
      type: 'group',
    },
    children: [
      {
        path: '/deviceStatus',
        element: lazyLoadComp(lazy(() => import('@/pages/DeviceStatus'))),
        handle: {
          icon: <Icon name="icon-icon-normal" />,
          title: '设备状态',
          openKey: 'deviceStatus',
        },
      },
      {
        path: '/utiAnalysis',
        element: lazyLoadComp(lazy(() => import('@/pages/utiAnalysis'))),
        handle: {
          icon: <Icon name="icon-icon-normal-1" />,
          title: '利用率分析',
          openKey: 'utiAnalysis',
        },
      },
      {
        path: '/systemConfig',
        element: lazyLoadComp(lazy(() => import('@/pages/systemConfig'))),
        handle: {
          icon: <Icon name="icon-config" />,
          title: '系统配置',
          openKey: 'systemConfig',
        },
      },
      {
        path: '/systemConfig/ruleConfig/:deviceCode',
        element: lazyLoadComp(
          lazy(() => import('@/pages/systemConfig/components/RuleConfig'))
        ),
        handle: {
          hide: true,
          title: '设备规则配置',
          menuKey: '/systemConfig',
          parent: {
            title: '采集配置',
            path: '/systemConfig?2',
          },
        },
      },
    ],
  },
];

export default statisticAnalysis;
