import { lazy } from 'react';
import type { RouteObject } from 'react-router-dom';
import lazyLoadComp from '../lazyComp';

const Layouts = lazy(() => import('@/layouts'));

const deviceMonitor: RouteObject[] = [
  {
    path: '/',
    element: lazyLoadComp(Layouts),
    children: [
      {
        path: '/',
        element: lazyLoadComp(Layouts),
      },
    ],
  },
];

export default deviceMonitor;
