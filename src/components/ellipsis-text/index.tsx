import { Tooltip } from 'antd';
import React, { useState } from 'react';
import styles from './index.module.scss';

const Ellipsis: React.FC<{
  content: string | React.ReactNode;
  wrapperClassName?: string;
  style?: React.CSSProperties;
}> = ({ content, wrapperClassName, ...restProps }) => {
  const [tooltipOpen, setToolTipsOpen] = useState(false);

  const handleMouseEnter = (event: any) => {
    if (event.target.clientWidth < event.target.scrollWidth) {
      setToolTipsOpen(true);
    } else {
      setToolTipsOpen(false);
    }
  };

  const handleMouseLeave = () => {
    setToolTipsOpen(false);
  };
  return (
    <div
      className={`${styles.ellipsisTextWrap} ${wrapperClassName}`}
      {...restProps}>
      <Tooltip
        title={content}
        placement="top"
        open={tooltipOpen}
        overlayStyle={{ maxWidth: '360px' }}>
        <div
          className={styles.ellipsisText}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}>
          {content}
        </div>
      </Tooltip>
    </div>
  );
};

export default Ellipsis;
