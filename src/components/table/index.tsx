/*
 * @Author: qingyuanyang
 * @Date: 2023-09-21 14:34:28
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-09 11:03:35
 * @FilePath: /utilization-platform/src/components/table/index.tsx
 * @Description: table组件
 */
import { Table, TableProps } from 'antd';
import classNames from 'classnames';
import { PropsWithChildren } from 'react';
import style from './index.module.scss';

interface Props extends TableProps<any> {
  isCustomizeHeaderColor?: boolean;
}

const DefaultTable: React.FC<Props> = ({
  isCustomizeHeaderColor = false,
  ...rest
}) => {
  const components = {
    body: {
      cell: (props: PropsWithChildren) => {
        const { children } = props;
        // 当表格数据为空时，显示'-'

        return Array.isArray(children) &&
          children.every((child: any) =>
            [undefined, null, ''].includes(child)
          ) ? (
          <td {...props}>-</td>
        ) : (
          <td {...props} />
        );
      },
    },
  };

  return (
    <Table
      {...rest}
      className={classNames(
        style.tableDiv,
        isCustomizeHeaderColor && style.tableCustomHeaderDiv
      )}
      components={components}
    />
  );
};
export default DefaultTable;
