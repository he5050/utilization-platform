/*
 * @Author: qingyuanyang
 * @Date: 2023-09-21 14:34:28
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-11 14:53:05
 * @FilePath: /utilization-platform/src/components/modal/index.tsx
 * @Description: modal组件
 */
import { Modal } from 'antd';
import { ModalProps } from 'antd/lib';
import style from './index.module.scss';

const DefaultModal: React.FC<ModalProps> = ({ ...rest }) => {
  return <Modal centered={true} {...rest} className={style.modalDiv} />;
};
export default DefaultModal;
