/*
 * @Author: yzr
 * @Date: 2023-09-19 15:21:06
 * @LastEditors: qingyuanyang
 * @LastEditTime: 2023-10-08 19:17:20
 * @Description: 自定义头部选择框
 * @FilePath: /utilization/src/components/BeforefixSelect/index.tsx
 */
import { Select } from 'antd';
import type { SelectProps } from 'antd/es/select';

import style from './index.module.scss';

interface Iprops extends SelectProps {
  beforefix: string;
}
const BeforefixSelect: React.FC<Iprops> = ({ beforefix, ...rest }) => {
  return (
    <div className={style.beforefixSelect} style={{ display: 'flex' }}>
      <span className={style.beforefix}>{beforefix}</span>
      <Select
        style={{ minWidth: '120px' }}
        getPopupContainer={triggerNode => triggerNode}
        {...rest}></Select>
    </div>
  );
};

export default BeforefixSelect;
