import type { CSSProperties } from "react"
import type { EChartsOption } from "echarts"

type Opts = {
  readonly devicePixelRatio?: number,
  readonly renderer?: 'svg' | 'canvas',
  readonly useDirtyRect?: boolean,
  readonly useCoarsePointer?: boolean,
  readonly pointerSize?: number,
  readonly ssr?: boolean,
  readonly width?: number|string,
  readonly height?: number|string,
  readonly locale?: string   
}
/**
 * https://echarts.apache.org/zh/api.html#echartsInstance.setOption
 */
export type ChartProps = {
  readonly className?: string,
  readonly chartStyle?: CSSProperties,
  readonly notMerge?: boolean,
  readonly replaceMerge?: string | string[]
  readonly lazyUpdate?: boolean,
  /**
   * options 图表具体内容的配置
   */
  readonly option: EChartsOption,
  readonly theme?: string | Record<string, any>
  /**
   * opts 图表表现形式的配置
   */
  readonly opts?: Opts
  readonly showLoading?: boolean
  readonly loadingOption?: any;
  readonly onEvents?: Record<string, Function>;
}