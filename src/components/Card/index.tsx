/*
 * @Author: yzr
 * @Date: 2023-09-19 09:50:43
 * @LastEditors: yzr
 * @LastEditTime: 2023-09-19 10:37:25
 * @Description: 通用卡片组件
 * @FilePath: /utilization/src/components/Card/index.tsx
 */
import { Tooltip } from 'antd';
import Icon from '@/components/SvgIcon';
import style from './index.module.scss';
interface Iprops {
  children: React.ReactNode;
  title?: string;
  tips?: string;
}

const Card: React.FC<Iprops> = ({ children, title, tips }) => {
  return (
    <div className={style.card}>
      {title && (
        <div className={style.title}>
          {title}
          {tips && (
            <Tooltip title={tips}>
              <Icon className={style.tips} name="icon-info2" />
            </Tooltip>
          )}
        </div>
      )}
      {children}
    </div>
  );
};

export default Card;
