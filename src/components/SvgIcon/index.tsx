/*
 * @Author: yzr
 * @Date: 2023-09-08 14:25:41
 * @LastEditors: yzr
 * @LastEditTime: 2023-10-10 16:55:15
 * @Description:
 * @FilePath: /utilization/src/components/SvgIcon/index.tsx
 */
import { createFromIconfontCN } from '@ant-design/icons';
import type { IconComponentProps } from '@ant-design/icons/lib/components/Icon';
const SvgIcon = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/c/font_4149551_uitwemjdkq.js',
});

type Props = IconComponentProps & {
  name: string;
};

const Icon: React.FC<Props> = ({ name, style, ...restProps }) => {
  let resultStyle = {
    fontSize: '20px',
    cursor: 'pointer',
    ...style,
  };
  return <SvgIcon type={name} {...restProps} style={resultStyle} />;
};
export default Icon;
